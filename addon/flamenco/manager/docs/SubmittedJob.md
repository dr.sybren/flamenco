# SubmittedJob

Job definition submitted to Flamenco.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | 
**type** | **str** |  | 
**priority** | **int** |  | defaults to 50
**settings** | [**JobSettings**](JobSettings.md) |  | [optional] 
**metadata** | [**JobMetadata**](JobMetadata.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


